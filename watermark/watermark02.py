# Watermark 02 commentary: 
# the coordinates are hidden in the same manner as in watermark level 01, 
# but not in the same place. you'll have to look around to find them. 
# when you do you'll realise there is too much information, 
# so you'll need to also find something to tell you what information to use. 
# emails welcome.

from PIL import Image
import numpy as np

for bit in range(8) :

    im = Image.open('level02.bmp')
    im = im.convert('RGB')
    width = im.size[0]
    height = im.size[1]
    pixels = im.load()

    for w in range(width) :
        for h in range(height) : 
            triple = pixels[w, h]
            newtpl = [0,0,0]
            for i in range(3) : 
                newtpl[i] = triple[i] & (1 << bit)
                newtpl[i] = newtpl[i] << (7-bit)
            
            pixels[w, h] = (newtpl[0], newtpl[1], newtpl[2])

    im.save("watermark02out{0}.bmp".format(bit))
