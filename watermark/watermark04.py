# Watermark 04 commentary: 
# rgb parity

from PIL import Image
import numpy as np

for bit in range(8) :

    im = Image.open('level04.bmp')
    im = im.convert('RGB')
    width = im.size[0]
    height = im.size[1]
    pixels = im.load()

    for w in range(width) :
        for h in range(height) : 
            triple = pixels[w, h]
            newtpl = [0,0,0]
            for i in range(3) : 
                newtpl[i] = triple[i] & (1 << bit)
                newtpl[i] = newtpl[i] >> bit
            
            val = newtpl[0] ^ newtpl[1] ^ newtpl[2]
            val = val << 7

            pixels[w, h] = (val, val, val)

    im.save("out/watermark04out{0}.bmp".format(bit))


