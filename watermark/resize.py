
from PIL import Image
import numpy as np

im = Image.open('out_lvl6/watermark06Gout0.bmp')
im = im.convert('RGB')
im = im.crop((0,300,600,450))

pixels = im.getdata()

totalsize = im.size[0] * im.size[1]
print(totalsize)

for size_x in range(100, 1200, 1) :
  
    size_y = totalsize / size_x
    print("Size of output: {0} x {1}".format(size_x, size_y))

    outim = Image.new('RGB', (int(size_x), int(size_y+10)), (0,0,0))
    outim.putdata(pixels)
    outim.save("out_lvl6/resized/resized{0}.bmp".format(size_x))



