# Watermark 06 commentary: 
# just because the source picture is six hundred by four hundred and fifty pixels, 
# doesn't mean the decoded image is too.


from PIL import Image
import numpy as np

for bit in range(8) :

    im = Image.open('level06.bmp')
    im = im.convert('RGB')
    width = im.size[0]
    height = im.size[1]
    pixels = im.load()

    for w in range(width) :
        for h in range(height) : 
            triple = pixels[w, h]
            newtpl = [0,0,0]
            for i in range(3) : 
                newtpl[i] = triple[i] & (1 << bit)
                newtpl[i] = newtpl[i] << (7-bit)
            
            pixels[w, h] = (newtpl[0], newtpl[1], newtpl[2])

    imR, imG, imB = im.split()
    im.save("out_lvl6/watermark06out{0}.bmp".format(bit))
    imR.save("out_lvl6/watermark06Rout{0}.bmp".format(bit))
    imG.save("out_lvl6/watermark06Gout{0}.bmp".format(bit))
    imB.save("out_lvl6/watermark06Bout{0}.bmp".format(bit))


