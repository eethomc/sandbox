from PIL import Image
import numpy as np

im = Image.open('level01.bmp')
im = im.convert('RGB')
width = im.size[0]
height = im.size[1]

print("{0} {1}".format(width, height))

pixels = im.load()

for w in range(width) :
    for h in range(height) : 
        triple = pixels[w, h]
        newtpl = [0,0,0]
        for i in range(3) : 
            newtpl[i] = triple[i] & (1 << 3)
            newtpl[i] = newtpl[i] << 4
        
        pixels[w, h] = (newtpl[0], newtpl[1], newtpl[2])


im.save("out.bmp")
im.show()