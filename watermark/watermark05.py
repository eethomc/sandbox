# Watermark 05 commentary: 
# compare two halves of the picture.

from PIL import Image
import numpy as np

for bit in range(8) :

    im = Image.open('level05.bmp')
    im = im.convert('RGB')
    width = im.size[0]
    height = im.size[1]
    pixels = im.load()

    for w in range(width) :
        for h in range(height) : 
            triple = pixels[w, h]
            comptriple = pixels[w, h]
            if h >= height/2 :
                comptriple = pixels[w, h - (height/2 + 1)]
            else : 
                comptriple = pixels[w, h + (height/2)]

            newtpl = [0,0,0]
            newcmptpl = [0,0,0]

            for i in range(3) : 
                newtpl[i] = triple[i] & (1 << bit)
                newtpl[i] = newtpl[i] >> bit
                newcmptpl[i] = comptriple[i] & (1 << bit)
                newcmptpl[i] = newcmptpl[i] >> bit

            
            val = newtpl[0] ^ newtpl[1] ^ newtpl[2]
            val = val << 7
            cmpval = newcmptpl[0] ^ newcmptpl[1] ^ newcmptpl[2]
            cmpval = cmpval << 7

            newval = val - cmpval
            
            pixels[w, h] = (newval, newval, newval)

    im.save("out/watermark05out{0}.bmp".format(bit))
    imR, imG, imB = im.split()
    imR.save("out/watermark05Rout{0}.bmp".format(bit))
    imG.save("out/watermark05Gout{0}.bmp".format(bit))
    imB.save("out/watermark05Bout{0}.bmp".format(bit))





