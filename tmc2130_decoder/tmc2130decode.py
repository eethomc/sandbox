import os

capture = open('capture.txt', 'r')
output = open('out.txt', 'w')

output.write("R/W\tMOSI\tMISO\n\n")

miso = ""
mosi = ""

firstpass = True
isMiso = False
isMosi = False

frameCounter = 0
isWrite = False
readCounter = 0
writeCounter = 0

while True: 
     
    c = capture.read(1)
    if not c:
        break
    
    if firstpass: 
        if c == ' ':
            firstpass = False
            isMosi = True
    else: 
        if c == ' ':
            isMosi = True
            isMiso = False

            output.write(miso + "\t" + "\n")

            if frameCounter == 0:
                output.write("\n")

            mosi = ""
        elif c == '/':
            isMiso = True
            isMosi = False

            if frameCounter == 0:

                mosiInt = int(mosi, 0)
                isWrite = mosiInt & 0x80
                
                if isWrite:
                    writeCounter += 1
                    mosiInt = mosiInt ^ 0x80
                    mosi = hex(mosiInt)
                else:
                    readCounter += 1
            
            if isWrite : 
                output.write("WRITE\t" + mosi + "\t")

            else:
                output.write("READ\t" + mosi + "\t")

            frameCounter += 1
            if frameCounter > 4:
                frameCounter = 0

            miso = ""
        else:
            if isMosi:
                mosi += c
            elif isMiso:
                miso += c
            else:
                print("ERROR, not MISO or MOSI")
                break     

print("File decoding finished")
print(f"Found {writeCounter} writes and {readCounter} reads")

