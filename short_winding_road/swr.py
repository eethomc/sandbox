import numpy as np 
import sys

def print_grid(gd): 

    # R: 
    print("Right:\n")

    # Print the grid
    for y in range(0,26) : 
        string = ""
        for x in range(0,26) : 
            string += "{:3d}  ".format(int(gd[x, y, 0]))

        print(string + '\n')

    # D: 
    print("Down:\n")

    # Print the grid
    for y in range(0,26) : 
        string = ""
        for x in range(0,26) : 
            string += "{:3d}  ".format(int(gd[x, y, 1]))

        print(string + '\n')

    # Total: 
    print("Total:\n")

    # Print the grid
    for y in range(0,26) : 
        string = ""
        for x in range(0,26) : 
            string += "{:3d}  ".format(int(gd[x, y, 2]))

        print(string + '\n')


def make_grid():
    pifile = open('pi.txt','r')
    pi=pifile.read()

    gd = np.zeros((26,26,3))

    i = 0
    for y in range(26) :
        for x in range(26) :
            
            # Will always be bigger than the distance
            gd[x,y,2] = 999

            # If we're not at the end column (can't go right from there)
            if x != 25 :
                gd[x,y,0] = pi[i]

                # Can't go down from z
                if y != 25 :
                    gd[x,y,1] = pi[i+25] # Not 26 as we don't go right on z
                else : 
                    gd[x,y,1] = 999
                i += 1
            else :

                gd[x,y,0] = 999

                # Can't go down from z
                if y != 25 :
                    gd[x,y,1] = pi[i+25] # Not 26 as we don't go right on z
                else :
                    gd[x,y,1] = 999
                i += 1

        i += 25 
    
    return gd

def find_dist(gd):
    for lim in range(0,26) :
        for a in range(lim) :

            # Perform update
            update_cell(gd, a, lim)

            # Perform update
            update_cell(gd, lim, a)

        update_cell(gd, lim, lim)

def update_cell(gd, x, y) :
    if x == 0 and y == 0 : 
        gd[x, y, 2] = 0
        return
    
    if x != 0 :
        # if the leftmost cell plus the link is less than the current
        if gd[x-1, y, 2] + gd[x-1, y, 0] < gd[x, y, 2] :
            gd[x, y, 2] = gd[x-1, y, 2] + gd[x-1, y, 0]

    if y != 0 : 
        # if the upper cell plus the link is less than the current
        if gd[x, y-1, 2] + gd[x, y-1, 1] < gd[x, y, 2] :
            gd[x, y, 2] = gd[x, y-1, 2] + gd[x, y-1, 1]
        
    # Check the remaining directions
    
    #Right
    if x != 25 : 
        if gd[x+1, y, 2] + gd[x, y, 0] < gd[x, y, 2] :
            gd[x, y, 2] = gd[x+1, y, 2] + gd[x, y, 0]

    #Down
    if y != 25 : 
        if gd[x, y+1, 2] + gd[x, y, 1] < gd[x, y, 2] :
            gd[x, y, 2] = gd[x, y+1, 2] + gd[x, y, 1]

    # If the cell updating affects any neighbouring cells (downstream) update them

    if x != 0 : 
        if gd[x-1, y, 2] > gd[x, y, 2] + gd[x-1, y, 0] : 
            update_cell(gd, x-1, y)
        
    if y != 0 : 
        if gd[x, y-1, 2] > gd[x, y, 2] + gd[x, y-1, 1] : 
            update_cell(gd, x, y-1)
        
    # Think we're done now?


grid = make_grid()
print_grid(grid)
print('\n')

find_dist(grid)
print_grid(grid)

print("N51\n")
print("{:d}\n".format(int(grid[2, 22, 2])))
print("{:d}\n".format(int(grid[15, 15, 2])))
print(".\n")
print("{:d}\n".format(int(grid[20, 11, 2])))
print("{:d}\n".format(int(grid[16, 25, 2])))
print("{:d}\n".format(int(grid[5, 4, 2])))

print("W000\n")
print("{:d}\n".format(int(grid[2, 15, 2])))
print("{:d}\n".format(int(grid[23, 18, 2])))
print(".\n")
print("{:d}\n".format(int(grid[25, 9, 2])))
print("{:d}\n".format(int(grid[9, 16, 2])))
print("{:d}\n".format(int(grid[23, 23, 2])))



