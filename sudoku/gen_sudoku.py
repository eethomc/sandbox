import sys
from random import randint, shuffle
import constraints

def emptyGrid() : 

    # Build the empty grid
    grid = []
    grid.append([0, 0, 0, 0, 0, 0, 0, 0, 0])
    grid.append([0, 0, 0, 0, 0, 0, 0, 0, 0])
    grid.append([0, 0, 0, 0, 0, 0, 0, 0, 0])
    grid.append([0, 0, 0, 0, 0, 0, 0, 0, 0])
    grid.append([0, 0, 0, 0, 0, 0, 0, 0, 0])
    grid.append([0, 0, 0, 0, 0, 0, 0, 0, 0])
    grid.append([0, 0, 0, 0, 0, 0, 0, 0, 0])
    grid.append([0, 0, 0, 0, 0, 0, 0, 0, 0])
    grid.append([0, 0, 0, 0, 0, 0, 0, 0, 0])

    return grid


# Print the grid
def printGrid(grid):
    for i in range(9):
        rowstr = ""
        for j in range(9):
            rowstr += "{0} ".format(grid[i][j])
        print(rowstr)
    return

# A function to check if the grid is full
def checkGrid(grid):
    for row in range(0, 9):
        for col in range(0, 9):
            if grid[row][col] == 0:
                return False

    # We have a complete grid!
    return True

# Checks that the number is not in the same row
def checkRow(grid, value, row, col) :
    if not(value in grid[row]): 
        return True
    return False

# Checks that the number is not in the same col
def checkCol(grid, value, row, col) :
    if not value in (grid[0][col], grid[1][col], grid[2][col], grid[3][col], grid[4][col], grid[5][col], grid[6][col], grid[7][col], grid[8][col]):
        return True
    return False

def checkSquare(grid, value, row, col) : 
    # Identify which of the 9 squares we are working on
    square = []

    if row < 3:
        if col < 3:
            square = [grid[i][0:3] for i in range(0, 3)]
        elif col < 6:
            square = [grid[i][3:6] for i in range(0, 3)]
        else:
            square = [grid[i][6:9] for i in range(0, 3)]
    elif row < 6:
        if col < 3:
            square = [grid[i][0:3] for i in range(3, 6)]
        elif col < 6:
            square = [grid[i][3:6] for i in range(3, 6)]
        else:
            square = [grid[i][6:9] for i in range(3, 6)]
    else:
        if col < 3:
            square = [grid[i][0:3] for i in range(6, 9)]
        elif col < 6:
            square = [grid[i][3:6] for i in range(6, 9)]
        else:
            square = [grid[i][6:9] for i in range(6, 9)]
            
    # Check that this value has not already be used on this 3x3 square
    if not value in (square[0] + square[1] + square[2]):
        return True

    return False

# recursively builds the grid, backtracking at a fruitless branch
def buildGrid(grid, seed, const):

    if grid[4][4] == 0: 
        grid[4][4] = randint(1,9)

    # Find next empty cell
    for i in range(0, 81):
        row = (i//9)
        col = (i % 9)

        # The number is empty
        if grid[row][col] == 0:

            #randomly shuffle the number list to avoid filling in a predictable way
            shuffle(seed)

            # Try each number
            for value in seed:

                # Have we used this in this row?
                if checkRow(grid, value, row, col) :

                    # Have we used this in this COL?
                    if checkCol(grid, value, row, col) :

                        # Have we used this in this SQUARE?
                        if checkSquare(grid, value, row, col) :
                            
                            # Check the constraints
                            if constraints.check(grid, value, row, col, const) : 

                                #Pass! Save the digit
                                grid[row][col] = value

                                #Is the grid full?
                                if checkGrid(grid):
                                    return True
                                else:

                                    # If, from this digit, we cannot fit a digit further down
                                    # the line, this will return False up the chain, retrying each
                                    # number until it reaches this number, at which point it will break
                                    # and try a different digit. Not the most efficient - but still quicker
                                    # than a true brute force. 
                                    if buildGrid(grid, seed, const):
                                        return True
            break

    grid[row][col] = 0
    return False

# Main function
if __name__ == '__main__':

    grid = emptyGrid()

    # Seed the sudoku to start
    seed = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    shuffle(seed)

    # Get the constraints from the command line
    const = constraints.genFromCommandLine(sys.argv)

    # They typed something wrong
    if "err" in const : 
        print("Error in usage, constraints not recognised...")
        print("Usage: python ./get_sudoku")
        for key, _, desc in constraints.constLUT : 
            print("       {0:<20} : {1}".format(key, desc))
    
    else : 

        # Generate a Fully Solved Grid
        if buildGrid(grid, seed, const) :
            print("Valid grid: ")
            printGrid(grid)
        else : 
            print("No valid grid found subject to constraints given.")
