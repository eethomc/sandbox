# Checks for the consecutive constraint
def checkConsecutive(grid, value, row, col) :
    values = [value + 1, value - 1]
    
    if row >= 1 : 
        if grid[row-1][col] != 0 and grid[row-1][col] in values : 
            return False

    if row <= 7 : 
        if grid[row+1][col] != 0 and grid[row+1][col] in values : 
            return False

    if col >= 1 : 
        if grid[row][col-1] != 0 and grid[row][col-1] in values : 
            return False

    if col <= 7 : 
        if grid[row][col+1] != 0 and grid[row][col+1] in values : 
            return False
    
    return True

# Checks for the consecutive constraint
def checkOrthDoubles(grid, value, row, col) :
    values = [value * 2 , value / 2]
    
    if row >= 1 : 
        if grid[row-1][col] != 0 and grid[row-1][col] in values : 
            return False

    if row <= 7 : 
        if grid[row+1][col] != 0 and grid[row+1][col] in values : 
            return False

    if col >= 1 : 
        if grid[row][col-1] != 0 and grid[row][col-1] in values : 
            return False

    if col <= 7 : 
        if grid[row][col+1] != 0 and grid[row][col+1] in values : 
            return False
    
    return True        

#Checks the knights move constraint
def checkKnightsMove(grid, value, row, col) :
    if col >= 1 :
        if row >= 2: 
            if grid[row-2][col-1] == value : 
                return False
        if row <= 6:
            if grid[row+2][col-1] == value : 
                return False
        if col >= 2:
            if row >= 1: 
                if grid[row-1][col-2] == value : 
                    return False
            if row <= 7:
                if grid[row+1][col-2] == value : 
                    return False
    if col <= 7 :
        if row >= 2: 
            if grid[row-2][col+1] == value : 
                return False
        if row <= 6:
            if grid[row+2][col+1] == value : 
                return False
        if col <= 6:
            if row >= 1: 
                if grid[row-1][col+2] == value : 
                    return False
            if row <= 7:
                if grid[row+1][col+2] == value : 
                    return False
    return True

#Checks the knights move constraint
def checkKingsMove(grid, value, row, col) :
    if row >= 1 : 
        if col >= 1 : 
            if grid[row-1][col-1] == value : 
                return False
        if col <= 7 : 
            if grid[row-1][col+1] == value : 
                return False
    if row <= 7 : 
        if col >= 1 : 
            if grid[row+1][col-1] == value : 
                return False
        if col <= 7 : 
            if grid[row+1][col+1] == value : 
                return False
    return True

def checkX(grid, value, row, col) : 
    
    # The upper diagonal
    if row == col : 

        values = []
        for i in range(9) : 
            if grid[i][i] != 0 : 
                values += [grid[i][i]]

        if value in values :
            return False

    # The lower diagonal
    if row + col == 8: 

        values = []
        for i in range(9) : 
            if grid[i][8-i] != 0 : 
                values += [grid[i][8-i]]

        if value in values :
            return False
    
    return True

# Constraints: 
# Constraints must be added in the format ["key", checkFunction, "description"]
# "key" - what to type in the command line to get this constraint [20 char limit]
# checkFunction - the name of the function which will check the constraint
#               - Must be of the form name(grid, value, row, col)
#               - Must return True if the constraint is satisfied and False if not
# "description" - A description of what the constraint is

constLUT = [
    ["-knights", checkKnightsMove, "No 2 cells a knights move apart may contain the same digit"],
    ["-kings", checkKingsMove, "No 2 cells a kings move apart may contain the same digit"],
    ["-consecutive", checkConsecutive, "No 2 orthogonally adjacent cells may contain consecutive digits"],
    ["-orthDoubles", checkOrthDoubles, "No to orthogonally adjacent cells may contain digits where one is double the other"],
    ["-Xgrid", checkX, "Digits may not repeat on the 2 diagonals of the sudoku"]
]

# Checks all the constraints
def check(grid, value, row, col, const) :
    
    # For every member of the constraints LUT
    for key, func, _ in constLUT : 

        # If the key is in the constraints given
        if key in const : 

            # Run the associated function
            if not func(grid, value, row, col) : 
                return False

    return True 

# Gets the constraints from the argv given and checks that no invalid constraints are given
def genFromCommandLine(argv) : 

    const = []

    # for every argument in the argv given
    for i, arg in enumerate(argv) : 
       
        #skip the first one
        if i != 0 : 
            valid = False

            # Does it appear in the constraint look up table
            for key, _, _ in constLUT :

                # It does!
                if key == arg : 

                    # Add the matching key to the constraints variable
                    const += [key]
                    valid = True
            
            # No matching constraint
            if not valid : 
                const += ["err"]
        
    return const