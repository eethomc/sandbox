import numpy as np 
import sys
import math

def rchampernowne(n):
    stri=""
    for i in range(1, n+1) :
        if stri.count(str(i))==0 : stri=stri+str(i)
    strb=""
    for c in stri :
        strb=strb+c+'\n'
    return strb

print("Writing the first " + sys.argv[1] + " elements of the sequence to file")

f = open('out.txt', 'w')
f.write(rchampernowne(int(sys.argv[1])))
f.close()

print("Done.")