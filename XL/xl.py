import numpy as np 
import sys
import itertools

def rowsum(gd, row) :
    return 100*gd[row, 0] + 10*gd[row, 1] + gd[row, 2]

def colsum(gd, col) :
    return 100*gd[0, col] + 10*gd[1, col] + gd[2, col]

grid = np.zeros((3, 3))
maxgrid = np
rowtotalmax = 0
coltotalmax = 0

for perms in list(itertools.permutations([1, 2, 3, 4, 5, 6, 7, 8, 9])) :
    
    grid[0:3,0] = perms[0:3]
    grid[0:3,1] = perms[3:6]
    grid[0:3,2] = perms[6:9]

    cont = False
    for row in range(3) :
        if rowsum(grid, row) % 11 != 0 : 
            cont = True
    
    if cont == False : 

        for col in range(3) :
            if colsum(grid, col) % 11 != 0 : 
                cont = True

        if cont == False : 

            print(grid)

            rowtotal = rowsum(grid, 0) + rowsum(grid, 1) + rowsum(grid, 2)
            coltotal = colsum(grid, 0) + colsum(grid, 1) + colsum(grid, 2)

            if coltotal > rowtotal :
                continue

            if rowtotal > rowtotalmax : 
                rowtotalmax = rowtotal
                coltotalmax = coltotal
                print("\n\nNew Max:")
                print(grid)
                print("Rows: {0} {1} {2}\nCols: {3} {4} {5}\nTotals: {6} {7}".format(rowsum(grid, 0), rowsum(grid, 1), rowsum(grid, 2), colsum(grid, 0), colsum(grid, 1), colsum(grid, 2), rowtotal, coltotal))
            
            if rowtotal == rowtotalmax :
                if coltotal > coltotalmax : 
                    rowtotalmax = rowtotal
                    coltotalmax = coltotal
                    print("\n\nNew Max:")
                    print(grid)
                    print("Rows: {0} {1} {2}\nCols: {3} {4} {5}\nTotals: {6} {7}".format(rowsum(grid, 0), rowsum(grid, 1), rowsum(grid, 2), colsum(grid, 0), colsum(grid, 1), colsum(grid, 2), rowtotal, coltotal))
