import numpy as np 
import sys
from itertools import permutations
import multiprocessing

# Translates an output co-ordinate into an input co-ordinate given a certain xy mapping
def translate(c, map) : 
    op = 0
    for i in range(10) : 
        n = (c >> map[i]) & 0x01
        if n == 1 : 
            op = op | (1 << i)

    return op
     
# Generates the screen for any given xy mapping
def generate(map) : 

    #print("Generating sequence with -\nx: ", map[0], map[1], map[2], map[3], map[4], map[5], map[6], map[7], map[8], map[9])

    # Make the output array
    output = np.zeros(shape=(64,16))

    # Open the input file
    inf = open('input.txt','r')
    
    # Read it into a big string we can index later
    instr = inf.read()

    # Iterate over the output space, computing the input location and checking it
    for ix in range(64) : 
        for iy in range(16) : 
            index = 64*iy+ix
            pos = translate(index,map)
            if instr[pos] == '*' :
                output[ix,iy] = 1

    # Close the file
    inf.close()

    return output

# Makes a string from the screen input
def makescreen(scr) :
    stri = "" 
    for iy in range (16) : 
        for ix in range (64) : 
            if scr[ix,iy] == 1 : 
                stri = stri + '*'
            else :
                stri = stri + ' '
        stri = stri + '\n'
    return stri

# Takes in a file of ascii and generates the numpy array from it
def genfromfile(fname) :

    # Make the output array
    output = np.zeros(shape=(64,16))

    # Open the input file
    inf = open(fname,'r')
    
    # Read it into a big string we can index later
    instr = inf.read()

    x = 0
    y = 0

    # Iterate over the input
    for i, v in enumerate(instr): 
        if v == '\n' :
            y += 1
            x = 0
        elif v == '*' :
            output[x, y] = 1
            x += 1
        elif v == ' ' :
            output[x, y] = 0
            x += 1 

     # Close the file
    inf.close()

    return output

def neighbours(arr) : 

    nonlinears = 0
    populars = 0
    orphans = 0

    # iterate over the entire array 
    for iy in range(16) : 
        for ix in range(64) :

            #if it is populated
            if arr[ix, iy] == 1 :

                # Find the bounds of the search area
                low_x = ix - 1
                
                if low_x < 0 :
                    low_x = 0

                low_y = iy - 1

                if low_y < 0 : 
                    low_y = 0

                high_x = ix + 1
                 
                if high_x >= 64 :
                    high_x = 63

                high_y = iy + 1

                if high_y >= 16 : 
                    high_y = 15

                # How many neigbours ? 
                found = 0

                # Search the area
                for iix in range(low_x, high_x+1) :
                    for iiy in range(low_y, high_y+1) :
                        if iix != ix or iiy != iy:
                            if arr[iix, iiy] == 1 :
                                found +=1
                    else :
                        continue
                    break

                
                if found == 0 : 
                    orphans += 1

                if found > 2:
                    nonlinears +=1

                if found > 5:
                    populars +=1         
    
    return orphans, nonlinears, populars

# program - do stuff with the permutation here
# perm - a list of permutations decided by the scheduler
# uid - thread unique id           
def prog(perm, uid) :

    id = 0
    wiring = [0,1,2,3,4,5,6,7,8,9]
    for p in perm :
        
        ###      Start permutation similarity        ###
        sim = 0
        for i in range(len(wiring)):
            if perm[i] == wiring[i]: sim += 1
        if sim > 6: #Select threshold here
            pass
        ###       End permutation similarity         ###
        
        else:
            out = generate(p)
            numorph, numnonlinear, numpopular = neighbours(out)
            
            if 0: 
                pass
            elif 0:
                pass
            else:
                # This converts the numpy array 'out' into an ascii render of the "screen"
                outstr = makescreen(out)
         
                l1='*****'
                l2='*    '
                l3='*****'
                l4='    '
                l5='*****'
          
                if outstr.find(l1) != 0:
                    pass
                elif outstr.find(l2,(1*64)+1) != (1*64)+1:
                    pass 
                elif outstr.find(l3,(2*64)+2) != (2*64)+2:
                    pass 
                elif outstr.find(l4,(3*64)+3) != (3*64)+3:
                    pass 
                elif outstr.find(l5,(4*64)+4) != (4*64)+4:
                    pass
                else: 

                    # Save to a file
                    fout = open("out/{0}_{1}.txt".format(uid,id), 'w')
                    fout.write(outstr)
                    fout.close()
                    
                    print("{0}_{1}".format(uid,id), end='\r')
        #print("{0}_{1}".format(uid,id), end='\r')
        id+=1

# Main function, starts "procs" number of processes and on the list - use prog for program flow
if __name__ == '__main__':
    procs = 8
    perm = list(permutations([0,1,2,3,4,5,6,7,8,9]))

    for i in range(procs) :
        perminput = perm[i*len(perm)//procs:((i+1)*len(perm)//procs)-1]
        print("Starting process {0} with range: {1} to {2}".format(i, i*len(perm)//procs, ((i+1)*len(perm)//procs)-1))
        p = multiprocessing.Process(target=prog, args=(perminput,i))
        p.start()
    